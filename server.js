var express = require('express'),
    winston = require('./config/winston'),
    path = require('path'),
    passport = require('./config/passport');
    
var app = express();     

winston.info('Started MEAN Application');

var db = require('./config/sequelize');

require('./config/express')(app, passport);

app.listen(3000);
winston.info('Server Started on port 3000');    