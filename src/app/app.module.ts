import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { LocalStorageModule } from 'angular-2-local-storage';
import { AppComponent } from './app.component';
import { EventsComponent } from './events/events.component';
import { appRoutes } from './app.routes';
import { NavComponent } from './nav/nav.component';
import { SignupComponent } from './signup/signup.component';
import { Error404Component } from './errors/404.component';
import { SigninComponent } from './signin/signin.component';
import { AuthActivator } from './auth/auth.activator';
import { NavService } from './nav/nav.service';
import { SigninService } from './signin/signin.service';

@NgModule({
  declarations: [
    AppComponent,
    EventsComponent,
    NavComponent,
    SignupComponent,
    Error404Component,
    SigninComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    LocalStorageModule.withConfig({
      prefix: 'app-root',
      // storageType: 'sessionStorage',
      storageType: 'localStorage',
    }),
  ],
  providers: [
    AuthActivator, 
    NavComponent,
    NavService,
    SigninService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
