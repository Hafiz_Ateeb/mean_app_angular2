import { Component, OnInit } from '@angular/core';
import { EventService } from './event.service';

@Component({
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
  providers: [EventService]
})
export class EventsComponent implements OnInit {

  events: any = [];

  constructor(private eventService: EventService) { 
    this.eventService.getEvents()
    .subscribe((res) => {
      this.events = res;
    })
  }

  ngOnInit() {
  }

  save(value){
    this.eventService.saveEvent(value)
    .subscribe((res) => {
      this.events.push(res);
      
    })
  }

  delete(id) {
    this.eventService.delEvent(id)
    .subscribe((res) => {
      var data = this.events.find(findId);
        var index = this.events.indexOf(data);
        this.events.splice(index, 1);
        this.events = this.events;
    });
    function findId(item) {
      return item.id === id;

    }
  }

}
