import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class EventService {
    constructor(private http: Http) {

    }

    saveEvent(data){
        let headers = new Headers({'Content-Type': 'application/json'})
        let options = new RequestOptions({headers: headers});

        return this.http.post('/api/event', data, options)
        .map(res => res.json());
    }

    getEvents() {
        return this.http.get('/api/event')
        .map(res => res.json());
    }

    delEvent(id) {
        return this.http.delete('/api/event/' + id)
        .map(res => res.json());
    }
}