import { Error404Component } from './errors/404.component';
import { Routes } from '@angular/router';
import { EventsComponent } from './events/events.component';
import { SignupComponent } from './signup/signup.component';
import { AuthActivator } from './auth/auth.activator';
import { SigninComponent } from './signin/signin.component';

export const appRoutes: Routes = [
    { path: 'signup', component: SignupComponent },
    { path: 'signin', component: SigninComponent },
    { path: 'event', component: EventsComponent, canActivate: [AuthActivator] },
    { path: '', redirectTo: '/event', pathMatch: 'full'},
    { path: '**', component: Error404Component}
    
]