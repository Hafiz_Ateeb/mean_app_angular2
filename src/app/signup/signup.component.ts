import { Component, OnInit } from '@angular/core';
import { SignupService } from './signup.service';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [SignupService]
})
export class SignupComponent implements OnInit {

  constructor(private signupService: SignupService, private router: Router, private localStorage: LocalStorageService ) { }

  ngOnInit() {
  }

  save(formValues){
    this.signupService.saveUser(formValues)
    .subscribe((res) => {
      if(res.status == 'success'){
        console.log('This is username', res);
        this.localStorage.set('username', res.username);
        this.router.navigate(['/event']);
      }
    })
  }

}
