import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class SignupService {

    constructor(private http: Http ){}

    saveUser(data){
        return this.http.post('/api/signup', data)
        .map(res => res.json());
    }

    
}