import { Component } from '@angular/core';

@Component({
    template: `
        <h1 class="error">Not Found</h1>
    `,
    styles: [`
        .error {
            font-size: 150px;
            text-align: center;
            margin-top: 150px;
        }
    `]
})
export class Error404Component {

}