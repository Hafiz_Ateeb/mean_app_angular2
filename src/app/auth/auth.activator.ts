import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { Http } from '@angular/http';
import { LocalStorageService } from 'angular-2-local-storage';
import { NavComponent } from '../nav/nav.component';

@Injectable()
export class AuthActivator implements CanActivate {

    constructor(private localStorage: LocalStorageService, private router: Router, private navComponent: NavComponent, private http: Http ){

    }

    canActivate(route: ActivatedRouteSnapshot){
        if(!this.localStorage.get('username')){
            this.navComponent.getUser()
            this.router.navigate(['/signin']);
        } else {
            this.navComponent.getUser();
            return true;
        }
        
    }

}