import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class NavService {

    constructor(private http: Http){
        
    }

    signOutUser(){
        return this.http.get('/api/signout')
        .map(res => res.json());
    }

    
}