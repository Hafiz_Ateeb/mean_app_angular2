import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { NavService } from './nav.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
})
export class NavComponent implements OnInit {

  navBar: boolean;

  constructor( private localStorage: LocalStorageService, private router: Router, private navService: NavService ) { 
  }

  getUser(){
    if(this.localStorage.get('username')){
            this.navBar = true;
      } else {
            this.navBar = false;
      }
  }
  

  ngOnInit() {
    this.getUser();
  }

  signout(){
    this.navService.signOutUser()
    .subscribe((res) => {
      if(res.status == 'success'){
        this.localStorage.remove('username');
        this.getUser();
        this.router.navigate(['/signin']);
      }
    })
    
  }


}
