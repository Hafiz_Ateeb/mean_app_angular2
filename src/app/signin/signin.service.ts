import { Observable } from '@angular-cli/ast-tools/node_modules/rxjs/Rx';
import { error } from 'util';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class SigninService {

    constructor(private http: Http){

    }

    signinUser(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.post('/api/signin', data, options)
        .map(res => {
            if(res){
                return res.json().username;
            }
        }).catch(error => {
            return Observable.of(false);
        })
    }

}