import { Component, OnInit } from '@angular/core';
import { SigninService } from './signin.service';
import { LocalStorageService } from 'angular-2-local-storage';
import { Router } from '@angular/router';

@Component({
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  invalidLogin = false;

  constructor(private signinService: SigninService, private localStorage: LocalStorageService, private router: Router) { }

  ngOnInit() {
  }

  save(formValues){
    this.signinService.signinUser(formValues)
    .subscribe(res => {
      if(res){
         this.localStorage.set('username', res);
          this.router.navigate(['/event']);
      } else {
        this.invalidLogin = true;
      }
    })
  }

}
