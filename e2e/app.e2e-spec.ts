import { AppMEANPage } from './app.po';

describe('app-mean App', function() {
  let page: AppMEANPage;

  beforeEach(() => {
    page = new AppMEANPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
