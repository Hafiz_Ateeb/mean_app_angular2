var path = require('path'),
    express = require('express'),
    flash = require('connect-flash'),
    winston = require('./winston'),
    router = express.Router(),
    cookieParser = require('cookie-parser'),
    sessionMiddleware = require('./middlewares/session'),
    bodyParser = require('body-parser');

 var api = require('../app/routes/event.route');  

module.exports = function(app, passport) {

    winston.info('Initializing Express');

    app.use(express.static(path.join(__dirname, '/../dist')));

    app.enable('josnp callback');

    app.use(cookieParser());

    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());

    app.use(sessionMiddleware);

    app.use(flash());

    app.use(passport.initialize());
    app.use(passport.session());

    require('../app/routes/index')(app, router);

}    