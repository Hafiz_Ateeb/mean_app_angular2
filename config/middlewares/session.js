var session = require('express-session'),
    db = require('../sequelize');

var sequelizeStore = require('express-sequelize-session')(session.Store);

var sessionMiddleware = session({
    resave: true,
    saveUninitialized: true,
    store: new sequelizeStore(db.sequelize),
    cookie: {maxAge: 1000*3600*24*7},
    secret: '$uper$ecret$e$$ionKey'
});

module.exports = sessionMiddleware;    