'use strict';

var fs = require('fs');
var path = require('path');
var Sequelize = require('sequelize');
var _ = require('lodash');
var winston = require('./winston');
var modelsDir = path.join(__dirname, '/../app/models');
var db = {};


winston.info('Initializing Sequelize...');

var dbForce = true;

// create your instance of sequelize
var onHeroku = !!process.env.DYNO;
winston.info('Checking if running on Heroku: ',onHeroku);

var sequelize =  onHeroku ?
    new Sequelize(process.env.DATABASE_URL, {
        dialect: 'postgres',
        protocol: 'postgres',
        dialectOptions: {
            ssl: true
        }
    })
    :
    new Sequelize('mean_app', 'root', 'root', {
        host: 'localhost',
        port: 3306,
        dialect: 'mysql',
    });

// loop through all files in models directory ignoring hidden files and this file
fs.readdirSync(modelsDir)
    .filter(function (file) {
        return (file.indexOf('.') !== 0) && (file !== 'index.js')
    })
    // import model files and save model names
    .forEach(function (file) {
        winston.info('Loading model file ' + file);
        var model = sequelize.import(path.join(modelsDir, file));
        db[model.name] = model;
    });

// invoke associations on each of the models
Object.keys(db).forEach(function (modelName) {
    if (db[modelName].options.hasOwnProperty('associate')) {
        db[modelName].options.associate(db)
    }
});

// Synchronizing any model changes with database. 
// set FORCE_DB_SYNC=true in the environment, or the program parameters to drop the database,
//   and force model changes into it, if required;
// Caution: Do not set FORCE_DB_SYNC to true for every run to avoid losing data with restarts

sequelize
    .sync({
        logging: console.log ? winston.verbose : false
    });

// assign the sequelize variables to the db object and returning the db. 
module.exports = _.extend({
    sequelize: sequelize,
    Sequelize: Sequelize
}, db);