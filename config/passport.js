var passport = require('passport'),
    db = require('./sequelize'),
    winston = require('./winston'),
    LocalStrategy = require('passport-local').Strategy;

passport.serializeUser(function(user, done){
    done(null, user.id);
});

passport.deserializeUser(function(id, done){
    db.User.find({where: {id: id}}).then(function(user){
        if(!user){
            winston.warn('Logged in user is not in database');
            return done(null, false);
        }
        winston.info('Session: { id: ' + user.id + ' ,username: ' + user.username + ' }');
        done(null, user);
    }).catch(function(err){
        done(null, err);
    });
});

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  function(email, password, done) {
    db.User.find({ where: { email: email }}).then(function(user) {
      if (!user) {
        return done(null, false, { message: 'Unknown user' });
      } else if (!user.authenticate(password)) {
        return done(null, false, { message: 'Invalid password'});
      } else {
        winston.info('Login (local) : { id: ' + user.id + ', username: ' + user.username + ' }');
        return done(null, user);
      }
    }).catch(function(err){
      done(err);
    });
  }
));

module.exports = passport;