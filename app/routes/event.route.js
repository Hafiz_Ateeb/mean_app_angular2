var event = require('../controllers/event')

module.exports = function(app, router) {
    
    router.route('/event')
    .get(event.all)
    .post(event.save);

    router.route('/event/:id')
    .delete(event.delete);


}