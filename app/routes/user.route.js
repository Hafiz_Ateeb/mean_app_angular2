var user = require('../controllers/user');
var passport = require('../../config/passport');

module.exports = function(app, router) {

    router.route('/signup')
    .post(user.create);

    router.get('/signout', user.signout);

    router.post('/signin', passport.authenticate('local', {
        failureRedirect: '/signin',
        failureFlash: true
    }), user.signin)

}