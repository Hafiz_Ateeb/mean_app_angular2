var path = require('path');

module.exports = function(app, router) {

    require('./user.route')(app, router);
    require('./event.route')(app, router);

    app.use('/api', router);
    app.get('*', (req, res) => {
        res.sendFile(path.join(__dirname, '/../../dist/index.html'));
    })
}