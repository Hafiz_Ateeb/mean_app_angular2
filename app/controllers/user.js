var db = require('../../config/sequelize');

exports.create = function (req, res, next) {
    var message = null;

    var user = db.User.build(req.body);

    user.provider = 'local';
    user.salt = user.makeSalt();
    user.hashedPassword = user.encryptPassword(req.body.password, user.salt);
    console.log('New User (local) : { id: ' + user.id + ' username: ' + user.username + ' }');

    user.save().then(function () {
        req.login(user, function (err) {
            if (err) {
                return next(err);
            }
            // res.redirect('/');
            return res.send({status: 'success', message: 'User signup successfully.', username: user.username});
             
        });
    }).catch(function (err) {
        res.render('users/signup', {
            message: message,
            user: user
        });
    });
};

exports.signout = function (req, res) {
    console.log('Logout: {id: ' + req.user.id + ' ,username: ' + req.user.username + ' } Successfully');
    req.logout();
    return res.send({status: 'success', message: 'User logout successfully'});
}

exports.signin = function (req, res) {
    return res.send({status: 'success', message: 'User logged in successfully', username: req.user.username});
}