var db = require('../../config/sequelize')

exports.all = function (req, res) {
    db.Event.findAll().then(function(data) {
        return res.jsonp(data);
    }).catch(function(err){
        return res.jsonp('This is error', err);
    })
}

exports.save = function (req, res) {
    db.Event.create(req.body).then(function (data) {
        return res.jsonp(data);
    }).catch(function (err) {
        return res.jsonp('This is error', err);
    });
}

exports.delete = function (req, res) {
    var id = req.params.id;

    db.Event.destroy({where: {id: id}}).then(function(data) {
        return res.jsonp(data);
    }).catch(function(err){
        return res.jsonp('This is error', err);
    })
}