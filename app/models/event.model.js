module.exports = function(sequelize, DataTypes) {
    var Event = sequelize.define('Event', {
        name: DataTypes.STRING,
        description: DataTypes.STRING,
    }, {
        associate: function(models){
            Event.belongsTo(models.User);
        }
    });
    return Event;
}